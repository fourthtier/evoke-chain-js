# Evoke Blockchain
A simple Blockchain app built with Node.js and deployed using Amazon Web Services

## Prerequisites
- node v8+
 
## Getting Started
- clone repo
- install dependencies
- run test suite
- fire the app up

```
git clone git@bitbucket.org:fourthtier/evoke-chain.git
cd evoke-chain
npm install

# run test suite
npm run test

# run app in dev mode
npm run dev
```


## Next steps: 

- Deploy to AWS with horizontal scaling
- Build client app (React+Redux)
